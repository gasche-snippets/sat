type var = int
type clause = int

type 'a var_array = 'a array
type 'a clause_array = 'a array

type formula = {
  clause_count : int ref;
    (* number of currently active clauses *)
  clause_active : bool clause_array;
    (* whether each clause is currently active *)
  clause_size : int ref clause_array;
    (* number of currently active atoms for each clause *)
  clauses : var_info var_array;
    (* per-variable info *)
}
and var_info = {
  var_active : bool clause_array;
    (* is the variable active in the clause *)
  polarity : bool clause_array;
    (* polarity of the variable in each clause *)
  neg_occurrences : int ref;
    (* number of negative [resp. positive] occurrences *)
  pos_occurrences : int ref;
}


let count_occurences = true

(* less redundant/efficient representations of clauses, for
   pretty-printing purposes *)
type repr = (int * bool) list list

let string_of_atom (x, v) =
  Printf.sprintf "%s%d" 
    (if v then "" else "¬") x

let string_of_clause clause =
  Printf.sprintf "(%s)"
    (String.concat " ∨ "
       (List.map string_of_atom clause))
    
let string_of_repr repr =
  Printf.sprintf "(%s)"
    (String.concat " ∧ "
       (List.map string_of_clause repr))

let repr_of_formula formula check =
  let clauses = ref [] in
  Array.iteri (fun clause active ->
    if active then begin
      let atoms = ref [] in
      Array.iteri (fun var info ->
        if info.var_active.(clause) then
          atoms := (var, info.polarity.(clause)) :: !atoms;
      ) formula.clauses;
      if check then
        assert (List.length !atoms = !(formula.clause_size.(clause)));
      clauses := List.rev !atoms :: !clauses;
    end) formula.clause_active;
  if check then begin
    assert (List.length !clauses = !(formula.clause_count));
    Array.iteri (fun x info ->
      let occur v = List.filter (List.mem (x,v)) !clauses in
      assert (!(info.pos_occurrences) = List.length (occur true));
      assert (!(info.neg_occurrences) = List.length (occur false));
    ) formula.clauses;
  end;
  List.rev !clauses

type var_delta = clause Stack.t

(* returns the list of clauses where a given variable is active,
   in view of removing the clause from them in [propagate]
*)
let record_active x formula =
  let record = ref [] in
  Array.iteri (fun clause active ->
    if active && formula.clause_active.(clause) then begin
      record := clause :: !record;
    end) formula.clauses.(x).var_active;
  !record

let occur var info clause =
  if info.polarity.(clause)
  then info.pos_occurrences
  else info.neg_occurrences

let unsat formula record =
  List.exists (fun clause ->
    formula.clause_active.(clause)
    && !(formula.clause_size.(clause)) = 0
  ) record

(* propagates the value of a variable along a record of concerned
   clauses; (mutates the formula) *)
let propagate debug x v formula record =
  if debug then
    Printf.eprintf "propagate %d %B\n" x v;
  if debug then
    assert (record_active x formula = record);
  let info = formula.clauses.(x) in
  List.iter (fun clause ->
    assert (formula.clause_active.(clause));
    let polarity = info.polarity.(clause) in
    info.var_active.(clause) <- false;
    decr (if polarity then info.pos_occurrences else info.neg_occurrences);
    if polarity = v then begin
      formula.clause_active.(clause) <- false;
      decr formula.clause_count;
      if count_occurences then Array.iteri (fun var info ->
        if var <> x && info.var_active.(clause) then
          decr (occur var info clause);
      ) formula.clauses;
    end else begin
      decr formula.clause_size.(clause);
    end
  ) record

(* cancels the propagation of a just-propagated variable value along
   a record of concerned clauses; we do assume that it was the last
   propagation (would not be correct in the general case);
   (mutates the formula)
*)
let backtrack debug x v formula record =
  if debug then
    Printf.eprintf "backtrack %d %B\n" x v;
  let info = formula.clauses.(x) in
  List.iter (fun clause ->
    let polarity = info.polarity.(clause) in
    info.var_active.(clause) <- true;
    incr (if polarity then info.pos_occurrences else info.neg_occurrences);
    if polarity = v then begin
      assert (not formula.clause_active.(clause));
      formula.clause_active.(clause) <- true;
      incr formula.clause_count;
      if count_occurences then Array.iteri (fun var info ->
        if var <> x && info.var_active.(clause) then
          incr (occur var info clause);
      ) formula.clauses;
    end else begin
      incr formula.clause_size.(clause);
    end
  ) record;
  if debug then
    assert (record_active x formula = record)

exception Unsat
exception Sat of (var * bool) list

let rec dpll debug env quant formula =
  if debug then
    prerr_endline (string_of_repr (repr_of_formula formula true));
  let det = ref [] in
  (* propagate determined-enough variables: if they have only positive
     occurrences, they can be instantiated to 'true' without any loss of
     satisfiability, and conversely for only negative occurrences *)
  if count_occurences then List.iter (fun x ->
    let info = formula.clauses.(x) in
    if !(info.neg_occurrences) = 0 then begin
      if debug then Printf.eprintf "var %d trivial neg\n%!" x;
      det := (x, true) :: !det;
    end else if !(info.pos_occurrences) = 0 then begin
      if debug then Printf.eprintf "var %d trivial pos\n%!" x;
      det := (x, false) :: !det;
    end;
  ) quant;
  (* propagate fully-determined clauses: if a clause has only one
     active atom let, satisfiability of the formula is only possible
     if the variable is assigned to the expected polarity *)
  Array.iteri (fun clause clause_active ->
    if clause_active && !(formula.clause_size.(clause)) = 1 then begin
      if debug then Printf.eprintf "clause %d trivial\n%!" clause;
      (* find the corresponding variable *)
      try Array.iteri (fun x info ->
        if info.var_active.(clause) then begin
          let v = info.polarity.(clause) in
          det := (x, v) :: !det;
          raise Exit
        end) formula.clauses
      with Exit -> ()
    end) formula.clause_active;
  if debug then 
    prerr_endline "phase: propagated determined"; 
  (* move the previously-determined from [quant] into [env] *)
  match !det with
    | _::_ ->
      (* propagate the determined variables, and then recursively call
         the solver in the same environment, to see if even more
         propagation is now possible *)
      begin
        let propagated = Stack.create () in
        try
          List.iter (fun (x,v) ->
            let record = record_active x formula in
            propagate debug x v formula (record_active x formula);
            if debug then
              prerr_endline (string_of_repr (repr_of_formula formula true));
            Stack.push (x,v,record) propagated;
            if unsat formula record then raise Unsat;
          ) !det;
          (* test to see if the formula is now satisfied *)
          if !(formula.clause_count) = 0 then
            raise (Sat env);
          if debug then
            prerr_endline "phase: pick instance";
          let quant = List.filter (fun x -> not (List.mem_assoc x !det)) quant in
          let env = List.rev_append !det env in
          dpll debug env quant formula
        with Unsat as exn ->
          Stack.iter (fun (x, v, record) ->
            backtrack debug x v formula record
          ) propagated;
          raise exn
      end
    | [] ->
      (* no variables were determined enough; pick one and try both
         instantions *)
      let x, rest =
        match quant with
          | [] ->
            (* there must remain some existential variables: if they had all
               been propagated, the formula would necessarily be solved, and
               the [clause_count = 0] test above would have passed. *)
            assert false 
             
          | _::_ when count_occurences && Random.int 5 = 0 ->
            let good v =
              let info = formula.clauses.(v) in
              min !(info.pos_occurrences) !(info.neg_occurrences) in
            let quant = List.sort (fun x y -> compare (good y) (good x)) quant in
            List.hd quant, List.tl quant

          | x::rest -> x, rest
      in
      let propagate_and_recurse v record =
        propagate debug x v formula record;
        (* test to see if the formula is now SAT or UNSAT,
           or instantiate the variable *)
        try
          if !(formula.clause_count) = 0 then raise (Sat env);
          if unsat formula record then raise Unsat;
          dpll debug ((x,v)::env) rest formula
        with Unsat as exn ->
          backtrack debug x v formula record;
          raise exn
      in
      (* DUMB:
         we always test with 'true' first and 'false' second,
         instead of thinking about which order could lead to
         a better search (eg. do the search killing the less
         clauses in the first, non-tail, call)
      *)
      let record = record_active x formula in
      try propagate_and_recurse true record
      with Unsat ->
        propagate_and_recurse false record

let any = true

(* (a \/ b \/ c) /\ (a \/ !b) /\ (!b \/ c) /\ (a \/ !c) *)
let formula () = {
  clause_count = ref 4;
  clause_active = [|true;true;true;true|];
  clause_size = [|ref 3; ref 2; ref 2; ref 2|];
  clauses = [|
   {(*a*)
      var_active = [|true;true;false;true|];
      polarity = [|true;true;any;true|];
      pos_occurrences = ref 3;
      neg_occurrences = ref 0;
   };
   {(*b*)
     var_active = [|true;true;true;false|];
     polarity = [|true;false;false;any|];
     pos_occurrences = ref 1;
     neg_occurrences = ref 2;
   };
   {(*c*)
     var_active = [|true;false;true;true|];
     polarity = [|true;any;true;false|];
     neg_occurrences = ref 1;
     pos_occurrences = ref 2;
   }
   |]
};;

let string_of_env env =
  Printf.sprintf "(%s)"
    (String.concat "," (List.map string_of_atom env))

let formula_of_armael (nb_vars, nb_clauses, clause_descrs) =
  let form =
    {
      clause_count = ref nb_clauses;
      clause_active = Array.make nb_clauses true;
      clause_size = Array.init nb_clauses (fun _ -> ref 0);
      clauses = Array.init nb_vars (fun _ -> {
        var_active = Array.make nb_clauses false;
        polarity = Array.make nb_clauses any;
        neg_occurrences = ref 0;
        pos_occurrences = ref 0;
      });
    } in
  let clause_descrs = ref clause_descrs in
  for clause = 0 to nb_clauses - 1 do
    let clause_vars = match !clause_descrs with
      | [] -> invalid_arg "formula_of_armael"
      | c::cs -> clause_descrs := cs; c in
    let clause_vars = List.map (fun x -> abs x - 1, x > 0) clause_vars in
    let polarities = Array.make nb_vars [] in
    (* sometimes a clause is present several time, with identical or
       distinct polarities, in the input file; if there is only one
       polarity, we write it, but if the two polarities are present we
       just ignore the clause: (a ‌∨ ¬a) is always true. *)
    List.iter (fun (x, v) ->
      if x < 0 then failwith "invalid input formula";
      if not (List.mem v polarities.(x))
      then polarities.(x) <- v :: polarities.(x)) clause_vars;
    if List.exists (fun (x, _) -> List.length polarities.(x) = 2) clause_vars
    then begin
      form.clause_active.(clause) <- false;
      decr form.clause_count;
    end else begin
      (* only one polarity per variable *)
      List.iter (fun (x, v) ->
        let info = form.clauses.(x) in
        (* may be already marked if (x,v) already appeared in clause_vars
           (for the same polarity) *)
        if not info.var_active.(clause) then begin
          info.var_active.(clause) <- true;
          incr form.clause_size.(clause);
          info.polarity.(clause) <- v;
          incr (occur x info clause);
        end
      ) clause_vars
    end
  done;
  ignore (repr_of_formula form true);
  form

let () =
  let form = formula_of_armael (Cnf.parse stdin) in
  let quant = (Array.init (Array.length form.clauses) (fun i -> i)) in
  (* let shuffled = Array.map (fun i -> (i, Random.float 1.)) quant in *)
  (* Array.sort (fun (_,w1) (_,w2) -> compare w1 w2) shuffled; *)
  (* let quant = Array.map fst shuffled in *)
  let quant = Array.to_list quant in
  try
    dpll false [] quant form
  with 
    | Unsat -> print_endline "UNSAT"
    | Sat env ->
      print_endline ("SAT " ^ string_of_env env)
        
